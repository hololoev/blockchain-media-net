project(bcmedianet)

set(EOSIO_WASM_OLD_BEHAVIOR "Off")
find_package(eosio.cdt)

add_contract( bcmedianet bcmedianet bcmedianet.cpp )
target_include_directories( bcmedianet PUBLIC ${CMAKE_SOURCE_DIR}/../include )
target_ricardian_directory( bcmedianet ${CMAKE_SOURCE_DIR}/../ricardian )