#include <bcmedianet.hpp>

ACTION bcmedianet::post( name user, std::string threadData ) {

  require_auth( user );

  double trxcost = 500; // 0.5 EOSIO
  eosio::asset quantity(trxcost, eosio::symbol("EOS", 4));

  std::string memo = "Post transaction payment";

  action act(
    permission_level{user, "active"_n},
    "eosio.token"_n, "transfer"_n,
    std::make_tuple( user, _self, quantity, memo ));
  act.send();
}

ACTION bcmedianet::comment( name user, std::string postTRXID, std::string parentTRXID, std::string commentData ) {

  require_auth( user );

  double trxcost = 125; // 0.125 EOSIO
  eosio::asset quantity(trxcost, eosio::symbol("EOS", 4));

  std::string memo = "Comment transaction payment";

  action act(
    permission_level{user, "active"_n},
    "eosio.token"_n, "transfer"_n,
    std::make_tuple( user, _self, quantity, memo ));
  act.send();
}

ACTION bcmedianet::vote( name user, std::string postTRXID, int voteValue ) {

  require_auth( user );

  double trxcost = 50; // 0.05 EOSIO
  eosio::asset quantity(trxcost, eosio::symbol("EOS", 4));

  std::string memo = "Vote transaction payment";

  action act(
    permission_level{user, "active"_n},
    "eosio.token"_n, "transfer"_n,
    std::make_tuple( user, _self, quantity, memo ));
  act.send();
}

EOSIO_DISPATCH( bcmedianet, (post)(comment)(vote) )
