
const React = require('react');
const Link = require('react-micro-router').Link;

const TITLE_LIMIT = 20;
const ABSTRACT_LIMIT = 135;

class PostItem extends React.Component {

  render() {

    let postBody = [{ type: 'text', body: 'Post body parse error'}];

    try {
      postBody = JSON.parse(this.props.data.body);
    } catch(err) {
      console.log('ERROR:', err);
    }

    let backGroundImage = '';
    for(let block of postBody) {
      if( block.type === 'image' ) {
        backGroundImage = block.body;
        break;
      }
    }

    let title = this.props.data.title.substr(0, TITLE_LIMIT);
    if( this.props.data.title.length > TITLE_LIMIT ) {
      title += '...';
    }

    let abstract = '';

    for(let block of postBody) {
      if( block.type == 'text' ) {
        abstract = block.body.substr(0, ABSTRACT_LIMIT) + '...';
        break;
      }
    }

    let postLink = '/post/' + this.props.data.blockchain_block_id + ':' + this.props.data.blockchain_trx_id;

    return(<div className="post-list-item">
      <div className="post-list-item-header" style={{'backgroundImage': `url('${backGroundImage}')`}}>
        <h3 title={this.props.data.title}><Link href={postLink}>{title}</Link></h3>
      </div>
      <div className="post-list-item-body">{abstract}</div>
      <div className="post-list-item-footer">
        <h6 className="card-subtitle mb-2 text-muted">{this.props.data.createdAt}</h6>
        <h6 className="card-subtitle mb-2 text-muted">by: {this.props.data.blockchain_user}</h6>
      </div>
    </div>);

  }
}

module.exports = PostItem;
