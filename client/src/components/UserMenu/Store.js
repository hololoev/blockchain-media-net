
const Axios = require('axios');
const Reflux = require('reflux');
const Router = require('react-micro-router');


const Actions = require('./Actions');
const config = require('../../../config');

class PageStore extends Reflux.Store {
  constructor() {
    super();

    this.state = {
      authorized: window.account.authorized,
      userName: window.account.user.eos_name || '',
      ddState: false
    };

    this.listenables = Actions;

    window.account.subscribe( () => {
      this.onAccountUpdate();
    });
  }

  onDdClick() {
    this.setState({ ddState: !this.state.ddState } );
  }

  onDdBackDropClick() {
    this.setState({ ddState: false } );
  }

  onLinkClick(href) {
    this.setState({ ddState: false } );
    Router.redirect(href);
  }

  onAccountUpdate() {
    if( window.account.authorized === true ) {
      this.setState({
        authorized: true,
        userName: window.account.user.eos_name
      });
    } else {
      this.setState({
        authorized: false,
        userName: ''
      });
    }
  }

  onSignOut() {
    this.setState({ ddState: false } );

    Axios({
      url: `${config.api.url}/auth`,
      method: 'delete',
      timeout: config.api.timeout,
      headers: {
        Authorization: 'Bearer ' + window.account.token
      }
    })
    .then( (result) => {
      window.account.unauthorize();
      Router.redirect('/');
    })
    .catch( (err) => {
      console.log('Server error:', err);
      window.account.unauthorize();
      Router.redirect('/');
    });
  }

}

module.exports = PageStore;