 
function getQueryParams() {
  let queryString = window.location.search;

  queryString = queryString.replace('?', ''); // replace only first ?

  let queryParts = queryString.split('&');
  let result = {};
  for(let part of queryParts) {
    let param = part.split('=');
    result[ param[ 0 ] ] = param[ 1 ];
  }

  return result;
}

module.exports = getQueryParams;
