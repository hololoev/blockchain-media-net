
const React = require('react');
const Reflux = require('reflux');

const Store = require('./Store');
const Actions = require('./Actions');

class TextBlock extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  deleteClick() {
    Actions.deleteBlock(this.props.blockIndex);
  }

  change(e) {
    Actions.textBodyChange(this.props.blockIndex, event.target.value);
  }

  render() {
    return(<div className="block-edit">
      <div className="text-right">
        <button className="btn btn-sm btn-danger" onClick={()=>this.deleteClick()}>X</button>
      </div>
      <textarea className="form-control text-block" value={this.props.blockData.body} onChange={(e)=>this.change(e)}></textarea>
    </div>)
  }
}

class ImageBlock extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  deleteClick() {
    Actions.deleteBlock(this.props.blockIndex);
  }

  change(e) {
    Actions.textBodyChange(this.props.blockIndex, event.target.value);
  }

  fileChanged(e) {
    let blockIndex = this.props.blockIndex;
    let reader = new FileReader();
    let fileName = e.target.files[ 0 ].name;

    reader.onload = function(e) {
      Actions.commentImageUpdate(blockIndex, e.srcElement.result, fileName);
    }

    reader.readAsDataURL(e.target.files[ 0 ]);
  }

  fileSelectClick(e) {
    if( e.target.children[ 0 ] )
      e.target.children[ 0 ].click();
  }

  positionClick(position) {
    Actions.imagePositionUpdate(this.props.blockIndex, position);
  }

  titleUpdate(e) {
    Actions.imageTitleUpdate(this.props.blockIndex, e.target.value);
  }

  render() {

    let leftFormattingClass = 'btn btn-secondary';
    let rightFormattingClass = 'btn btn-secondary';
    let wideFormattingClass = 'btn btn-secondary';
    let centerFormattingClass = 'btn btn-secondary';
    let imagePositionClass = 'block-image-left';

    if( this.props.blockData.align == 'left' ) {
      leftFormattingClass += 'btn btn-primary';
      imagePositionClass = 'block-image-left';
    }

    if( this.props.blockData.align == 'right' ) {
      rightFormattingClass += 'btn btn-primary';
      imagePositionClass = 'block-image-right';
    }

    if( this.props.blockData.align == 'wide' ) {
      wideFormattingClass += 'btn btn-primary';
      imagePositionClass = 'block-image-wide';
    }

    if( this.props.blockData.align == 'center' ) {
      centerFormattingClass += 'btn btn-primary';
      imagePositionClass = 'block-image-center';
    }

    if( this.props.blockData.body ) {
      return(<div className="block-edit image-block-edit">
        <div className="image-block-edit-back">
          <div className={imagePositionClass}>
            <img src={this.props.blockData.body} />
          </div>
          <div className="row image-block-bottom-menu">
            <div className="col-md-8">
              <input
                type="text"
                className="form-control"
                onChange={(e)=>{this.titleUpdate(e)}}
                value={this.props.blockData.title}
                placeholder="Image description"
              />
            </div>
            <div className="col-md-4 text-right">
              <button className={leftFormattingClass} onClick={()=>{this.positionClick('left')}}>&larr;</button>
              <button className={rightFormattingClass} onClick={()=>{this.positionClick('right')}}>&rarr;</button>
              <button className={wideFormattingClass} onClick={()=>{this.positionClick('wide')}}>&harr;</button>
              <button className={centerFormattingClass} onClick={()=>{this.positionClick('center')}}>&uarr;</button>
            </div>
          </div>
        </div>
        <div className="block-menu text-right">
          <button className="btn btn-sm btn-danger" onClick={()=>this.deleteClick()}>X</button>
        </div>
      </div>)
    } else {
      return(<div className="block-edit image-block-edit">
        <div className="image-block-edit-back">
          <h3 className="text-center text-muted" onClick={this.fileSelectClick}>Click me to select image ...
          <input
            type="file"
            className="hidden"
            onChange={(e) => this.fileChanged(e)}
            accept="image/*"
          /></h3>
        </div>
        <div className="block-menu text-right">
          <button className="btn btn-sm btn-danger" onClick={()=>this.deleteClick()}>X</button>
        </div>
      </div>)
    }
  }
}

class CreateBlockMenu extends Reflux.Component {

  buttonClick(blokType) {
    Actions.addBlock(blokType);
  }

  render() {
    return(<div className="">
      <span></span>
      <button className="btn btn-secondary" onClick={()=> this.buttonClick('text')}>+ Text</button>
      <button className="btn btn-secondary" onClick={()=> this.buttonClick('image')}>+ Image</button>
      <button className="btn btn-secondary disabled">+ Video</button>
      <button className="btn btn-secondary disabled">+ Audio</button>
    </div>)
  }
}


class PostEdit extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  titleChange(e) {
    Actions.titleChange(event.target.value);
  }

  render() {
    let contentBlocks = [];
    for(let index in this.state.postBlocks) {
      if( this.state.postBlocks[ index ].type == 'text' )
        contentBlocks.push(<TextBlock key={index} blockIndex={index} blockData={this.state.postBlocks[ index ]} />);

      if( this.state.postBlocks[ index ].type == 'image' )
        contentBlocks.push(<ImageBlock key={index} blockIndex={index} blockData={this.state.postBlocks[ index ]} />);

    }

    return (<div>

      <div className="form-group">
        <label>Post title</label>
        <input
          type="text"
          className="form-control"
          value={this.state.postTitle}
          onChange={(e)=>this.titleChange(e)}
        />
      </div>

      {contentBlocks}
      <CreateBlockMenu />

    </div>)
  }
}

module.exports = PostEdit;
