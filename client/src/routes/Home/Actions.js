
const Reflux = require('reflux');

const Actions = Reflux.createActions([
  'loadFreshPost',
]);

module.exports = Actions;
