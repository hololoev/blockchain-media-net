
const React = require('react');
const Reflux = require('reflux');

const Store = require('./Store');
const Actions = require('./Actions');

const PostItemBlock = require('../../components/PostItemBlock');

class Page extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  componentDidMount() {
    Actions.loadFreshPost();
  }

  render() {

    return (<div>
      <PostItemBlock
        title="Fresh publications"
        items={this.state.freshPosts}
        errors={this.state.freshPostsErrors}
        inprogress={this.state.freshPostsInprogress}
      />
    </div>)
  }
}

module.exports = Page;
