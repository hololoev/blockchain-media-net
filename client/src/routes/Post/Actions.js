
const Reflux = require('reflux');

const Actions = Reflux.createActions([
  'loadPost',
]);

module.exports = Actions;
