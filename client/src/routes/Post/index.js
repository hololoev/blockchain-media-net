
const React = require('react');
const Reflux = require('reflux');

const Store = require('./Store');
const Actions = require('./Actions');

const PostView = require('../../components/PostView');
const PostComments = require('./Comments');

class Page extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  componentDidMount() {
    Actions.loadPost();
  }

  render() {
    if( this.state.errors.length > 0 ) {
      let errors = [];
      for(let index in this.state.errors)
        errors.push(<div className="alert alert-warning" key={'error' + index}><strong>ERROR!</strong> {this.state.errors[ index ]}</div>);

      return(<div>
        {errors}
      </div>)
    }

    let bodyBlocks = [{ type: 'text', body: 'Post body parse error'}];

    try {
      bodyBlocks = JSON.parse(this.state.postData.body);
    } catch(err) {
      console.log('ERROR:', err);
    }

    return (<div>
      <PostView
        postTitle={this.state.postData.title}
        postBlocks={bodyBlocks}
      />

      <PostComments />

    </div>)
  }
}

module.exports = Page;
