
const Reflux = require('reflux');

const Actions = Reflux.createActions([
  'loadProfile',
  'loadBlockchainInfo',
  'updateProfile',
  'changePassword'
]);

module.exports = Actions;
