
const Axios = require('axios');
const Reflux = require('reflux');
const Router = require('react-micro-router');

const Actions = require('./Actions');
const config = require('../../../../config');

class PageStore extends Reflux.Store {
  constructor() {
    super();

    this.state = {
      errors: [],
      formInprogress: false,
      postsLoad: false,
      posts: []
    };

    this.listenables = Actions;
  }

  onLoadPosts() {
    this.setState({ formInprogress: true });
    Axios({
      url: `${config.api.url}/post?username=${window.account.user.eos_name}`,
      method: 'get',
      timeout: config.api.timeout
    })
    .then( (result) => {
      this.setState({ errors: [], formInprogress: false, posts: result.data.data });
    })
    .catch( (err, data) => {
      if( err.response && err.response.data )
        return this.setState({ errors: [ err.response.data.meta.error.message ], formInprogress: false });

      return this.setState({ errors: [ err ], formInprogress: false });
    });
  }

}

module.exports = PageStore;