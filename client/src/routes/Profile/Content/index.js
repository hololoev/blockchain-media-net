
const React = require('react');
const Reflux = require('reflux');

const Store = require('./Store');
const Actions = require('./Actions');

const PostItemBlock = require('../../../components/PostItemBlock');

class Page extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  componentDidMount() {
    Actions.loadPosts();
  }

  render() {

    return (<div>
      <PostItemBlock
        title="My publications"
        items={this.state.posts}
        errors={this.state.errors}
        inprogress={this.state.formInprogress}
      />
    </div>)
  }
}

module.exports = Page;
