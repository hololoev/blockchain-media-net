
const Axios = require('axios');
const Reflux = require('reflux');
const Router = require('react-micro-router');

const Actions = require('./Actions');
const config = require('../../../config');

const MIN_PASS_LENGTH = 3;

class PageStore extends Reflux.Store {
  constructor() {
    super();

    this.state = {
      errors: [],
      bchErrors: [],
      formInprogress: false,
      bchInfoInprogress: false,
      profile: {},
      blockchainInfo: {}
    };

    this.listenables = Actions;
  }

  onLoadBlockchainInfo() {
    this.setState({ bchInfoInprogress: true });
    Axios({
      url: `${config.api.url}/user/blockchainInfo`,
      method: 'get',
      timeout: config.api.timeout,
      headers: {
        Authorization: 'Bearer ' + window.account.token
      }
    })
    .then( (result) => {
      window.account.updateBlockchainInfo(result.data.data[ 0 ]);
      this.setState({ bchErrors: [], bchInfoInprogress: false, blockchainInfo: result.data.data[ 0 ] });
    })
    .catch( (err, data) => {
      if( err.response )
        if( err.response.status == 401 ) {
          this.setState({ errors: [ err.response.data.meta.error.message ], formInprogress: false });
          return Router.redirect('/sign_in');
        }


      if( err.response && err.response.data )
        return this.setState({ bchErrors: [ err.response.data.meta.error.message ], bchInfoInprogress: false });

      return this.setState({ bchErrors: [ err ], bchInfoInprogress: false });
    });
  }

  onLoadProfile() {
    this.setState({ formInprogress: true });
    Axios({
      url: `${config.api.url}/user`,
      method: 'get',
      timeout: config.api.timeout,
      headers: {
        Authorization: 'Bearer ' + window.account.token
      }
    })
    .then( (result) => {
      this.setState({ errors: [], formInprogress: false, profile: result.data.data[ 0 ] });
      this.onLoadBlockchainInfo();
    })
    .catch( (err, data) => {
      if( err.response )
        if( err.response.status == 401 ) {
          this.setState({ errors: [ err.response.data.meta.error.message ], formInprogress: false });
          return Router.redirect('/sign_in');
        }

      if( err.response && err.response.data )
        return this.setState({ errors: [ err.response.data.meta.error.message ], formInprogress: false });

      return this.setState({ errors: [ err ], formInprogress: false });
    });
  }

  checkPasswForm(form) {
    let errors = [];

    if( form.password.length < MIN_PASS_LENGTH )
      errors.push('Password is to short');

    if( form.password !== form.confirmation )
      errors.push('Password and confirmation do not match');

    return errors;
  }

  onChangePassword(form) {
    let formError = this.checkPasswForm(form);

    if( formError.length > 0 ) {
      return this.setState({ errors: formError });
    }

    this.setState({ formInprogress: true });

    Axios({
      url: `${config.api.url}/user/password`,
      method: 'put',
      timeout: config.api.timeout,
      headers: {
        Authorization: 'Bearer ' + window.account.token
      },
      data: form
    })
    .then( (result) => {
      this.setState({ errors: [], formInprogress: false, profile: result.data.data[ 0 ] });
    })
    .catch( (err, data) => {
      if( err.response && err.response.data )
        if( typeof err.response.data == 'object' )
          return this.setState({ errors: [ err.response.data.meta.error.message ], formInprogress: false });

      return this.setState({ errors: [ err.response.statusText ], formInprogress: false });
    });
  }

  checkEOSForm(form) {
    let errors = [];

    if( !form.eos_name )
      errors.push('EOS name cannot be empty');

    if( !form.eos_public )
      errors.push('EOS public key cannot be empty');

    if( !form.eos_private )
      errors.push('EOS private key cannot be empty');

    return errors;
  }

  onUpdateProfile(form) {
    let formError = this.checkEOSForm(form);

    if( formError.length > 0 ) {
      return this.setState({ errors: formError });
    }

    this.setState({ formInprogress: true });

    Axios({
      url: `${config.api.url}/user`,
      method: 'put',
      timeout: config.api.timeout,
      headers: {
        Authorization: 'Bearer ' + window.account.token
      },
      data: form
    })
    .then( (result) => {
      this.setState({ errors: [], formInprogress: false, profile: result.data.data[ 0 ] });
    })
    .catch( (err, data) => {
      if( err.response && err.response.data )
        if( typeof err.response.data == 'object' )
          return this.setState({ errors: [ err.response.data.meta.error.message ], formInprogress: false });

      return this.setState({ errors: [ err.response.statusText ], formInprogress: false });
    });
  }

}

module.exports = PageStore;