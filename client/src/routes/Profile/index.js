
const React = require('react');
const Reflux = require('reflux');

const Store = require('./Store');
const Actions = require('./Actions');

class BlockchainInfo extends React.Component {

  render() {
    if( this.props.inprogress || !Object.keys(this.props.data).length )
      return(<div key="preloader" className="preloader"><img src="/img/ajax-loader.gif" /></div>)

    let errors = [];
    for(let index in this.props.errors)
      errors.push(<div className="alert alert-warning" key={'error' + index}><strong>ERROR!</strong> {this.props.errors[ index ]}</div>);

    return(<div>
      {errors}

      <h3>Summary</h3>
      <table className="table table-striped">
        <tbody>
          <tr>
            <td>core liquid balance</td>
            <td>{this.props.data.core_liquid_balance}</td>
          </tr>
          <tr>
            <td>ram quota</td>
            <td>{this.props.data.ram_quota}</td>
          </tr>
          <tr>
            <td>net weight</td>
            <td>{this.props.data.net_weight}</td>
          </tr>
          <tr>
            <td>net weight</td>
            <td>{this.props.data.net_weight}</td>
          </tr>
          <tr>
            <td>ram usage</td>
            <td>{this.props.data.ram_usage}</td>
          </tr>
        </tbody>
      </table>

      <h3>Net limit</h3>
      <table className="table table-striped">
        <tbody>
          <tr>
            <td>used</td>
            <td>{this.props.data.net_limit.used}</td>
          </tr>
          <tr>
            <td>available</td>
            <td>{this.props.data.net_limit.available}</td>
          </tr>
          <tr>
            <td>max</td>
            <td>{this.props.data.net_limit.max}</td>
          </tr>
        </tbody>
      </table>

      <h3>Cpu limit</h3>
      <table className="table table-striped">
        <tbody>
          <tr>
            <td>used</td>
            <td>{this.props.data.cpu_limit.used}</td>
          </tr>
          <tr>
            <td>available</td>
            <td>{this.props.data.cpu_limit.available}</td>
          </tr>
          <tr>
            <td>max</td>
            <td>{this.props.data.cpu_limit.max}</td>
          </tr>
        </tbody>
      </table>

      <h3>Total resources</h3>
      <table className="table table-striped">
        <tbody>
          <tr>
            <td>owner</td>
            <td>{this.props.data.total_resources.owner}</td>
          </tr>
          <tr>
            <td>net weight</td>
            <td>{this.props.data.total_resources.net_weight}</td>
          </tr>
          <tr>
            <td>cpu weight</td>
            <td>{this.props.data.total_resources.cpu_weight}</td>
          </tr>
          <tr>
            <td>ram bytes</td>
            <td>{this.props.data.total_resources.ram_bytes}</td>
          </tr>
        </tbody>
      </table>
    </div>)
  }
}

class Page extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  componentDidMount() {
    Actions.loadProfile();
  }

  updateProfileClick(e) {
    e.preventDefault();
    e.stopPropagation();

    let form = {
      eos_name: document.getElementById('InputEOSName').value,
      eos_public: document.getElementById('InputEOSPubKey').value,
      eos_private: document.getElementById('InputEOSPrivKey').value
    }

    Actions.updateProfile(form);
  }

  changePassworClick(e) {
    e.preventDefault();
    e.stopPropagation();

    let form = {
      password: document.getElementById('InputPassword').value,
      confirmation: document.getElementById('InputPasswordConfirmation').value,
      currentPassword: document.getElementById('InputCurrentPassword').value
    }

    Actions.changePassword(form);
  }

  render() {

    let errors = [];
    let preloader = [];
    for(let index in this.state.errors)
      errors.push(<div className="alert alert-warning" key={'error' + index}><strong>ERROR!</strong> {this.state.errors[ index ]}</div>);

    if(this.state.formInprogress)
      preloader.push(<div key="preloader" className="preloader"><img src="/img/ajax-loader.gif" /></div>);

    return (<div>
      <h1 className="text-center">My profile</h1>
      <br/>
      <div className="row">
        <div className="col-sm-6 preloader-container">
          {errors}

            <div className="form-group">
              <label>Email address</label>
              <input type="email" className="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="pupkin@gmail.com" disabled="disabled"/>
            </div>

          <form>
            <h3>Change password</h3>

            <div className="form-group">
              <label>Current password</label>
              <input type="password" className="form-control" id="InputCurrentPassword" placeholder="**********" />
            </div>

            <div className="form-group">
              <label>New password</label>
              <input type="password" className="form-control" id="InputPassword" placeholder="**********" />
            </div>

            <div className="form-group">
              <label>Confirmation</label>
              <input type="password" className="form-control" id="InputPasswordConfirmation" aria-describedby="passConfirmHelp" placeholder="**********" />
              <small id="passConfirmHelp" className="form-text text-muted">Use strong password.</small>
            </div>

            <button className="btn btn-primary btn-block" onClick={this.changePassworClick}>Change password</button>
          </form>
          <br/><hr/><br/>
          <form>
            <h3>Update EOS profile</h3>

            <div className="form-group">
              <label>EOS Account name</label>
              <input type="text" className="form-control" id="InputEOSName" placeholder="pupkin.eosio" defaultValue={this.state.profile.eos_name} />
            </div>

            <div className="form-group">
              <label>EOS public key</label>
              <input type="text" className="form-control" id="InputEOSPubKey" placeholder="EOS8DorxGMFqPWd8DuCFMv9MtiuCQsj2WkmXfFPpZY15xFmwxLg5s" defaultValue={this.state.profile.eos_public} />
            </div>

            <div className="form-group">
              <label>EOS private key</label>
              <input type="text" className="form-control" id="InputEOSPrivKey" placeholder="5JGwstdCwyiEh7pCfPGcRdqFcR8ZW1b2cpTjwE8TWRF15Xz3BFb" defaultValue={this.state.profile.eos_private} />
            </div>

            <button className="btn btn-primary btn-block" onClick={this.updateProfileClick}>Update EOS profile</button>
          </form>

          {preloader}
        </div>

        <div className="col-sm-6">
          <BlockchainInfo
            data={this.state.blockchainInfo}
            inprogress={this.state.bchInfoInprogress}
            errors={this.state.bchErrors}
          />
        </div>

      </div>
    </div>)
  }
}

module.exports = Page;
