
const Axios = require('axios');
const Reflux = require('reflux');


const Actions = require('./Actions');
const config = require('../../../config');

class PageStore extends Reflux.Store {
  constructor() {
    super();

    this.state = {
      freshPostsErrors: [],
      freshPostsInprogress: false,
      freshPosts: [],
      queryString: '',
    };

    this.listenables = Actions;
  }

  onLoadSearchPost(queryString) {

    if( !queryString )
      return;

    if( queryString === this.state.queryString )
      return;

    this.setState({ freshPostsInprogress: true, queryString: queryString });

    Axios({
      url: `${config.api.url}/post?query=${queryString}`,
      method: 'get',
      timeout: config.api.timeout
    })
    .then( (result) => {
      this.setState({ freshPostsErrors: [], freshPostsInprogress: false, freshPosts: result.data.data });
    })
    .catch( (err, data) => {
      if( err.response && err.response.data )
        return this.setState({ freshPostsErrors: [ err.response.data.meta.error.message ], freshPostsInprogress: false });

      return this.setState({ freshPostsErrors: [ err ], freshPostsInprogress: false });
    });
  }
}

module.exports = PageStore;