
const Axios = require('axios');
const Reflux = require('reflux');
const Router = require('react-micro-router');

const Actions = require('./Actions');
const config = require('../../../config');

const MIN_PASS_LENGTH = 3;

class PageStore extends Reflux.Store {
  constructor() {
    super();

    this.state = {
      errors: [],
      formInprogress: false
    };

    this.listenables = Actions;
  }

  checkForm(form) {
    let errors = [];

    if( form.password.length < MIN_PASS_LENGTH )
      errors.push('Password is to short');

    if( form.email.length < 3 )
      errors.push('Wrong email');

    return errors;
  }

  async onSignIn(form) {
    let formError = this.checkForm(form);

    if( formError.length > 0 ) {
      return this.setState({ errors: formError });
    }

    this.setState({ errors: [], formInprogress: true });

    Axios({
      url: `${config.api.url}/auth`,
      method: 'post',
      data: form,
      timeout: config.api.timeout,
      //headers: {}
    })
    .then( (result) => {
      this.setState({ errors: [], formInprogress: false });
      window.account.authorize(result.data.data[ 0 ]);
      Router.redirect('/');
    })
    .catch( (err, data) => {

      if( err.response && err.response.data )
        if( typeof err.response.data == 'object' )
          return this.setState({ errors: [ err.response.data.meta.error.message ], formInprogress: false });

      return this.setState({ errors: [ err ], formInprogress: false });
    });
  }
}

module.exports = PageStore;