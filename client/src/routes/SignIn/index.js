
const React = require('react');
const Reflux = require('reflux');
const Link = require('react-micro-router').Link;

const Store = require('./Store');
const Actions = require('./Actions');

class Page extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  signinClick(e) {
    e.preventDefault();
    e.stopPropagation();

    let form = {
      email: document.getElementById('InputEmail').value,
      password: document.getElementById('InputPassword').value
    }

    Actions.signIn(form);
  }

  render() {

    let errors = [];
    let preloader = [];
    for(let index in this.state.errors)
      errors.push(<div className="alert alert-warning" key={'error' + index}><strong>ERROR!</strong> {this.state.errors[ index ]}</div>);

    if(this.state.formInprogress)
      preloader.push(<div key="preloader" className="preloader"><img src="/img/ajax-loader.gif" /></div>);

    return (<div>
      <h1 className="text-center">Sign in</h1>
      <br/>
      <div className="row">
        <div className="col-sm-6 preloader-container">
          {errors}
          <form>
            <div className="form-group">
              <label>Email address</label>
              <input type="email" className="form-control" id="InputEmail" placeholder="pupkin@gmail.com" disabled={this.state.formInprogress} />
            </div>

            <div className="form-group">
              <label>Password</label>
              <input type="password" className="form-control" id="InputPassword" placeholder="**********" disabled={this.state.formInprogress} />
            </div>

            <div className="row">
              <div className="col-sm-6">
                <Link href="/registration" className="btn btn-secondary btn-block">Register</Link>
              </div>

              <div className="col-sm-6 text-right">
                <button className="btn btn-primary btn-block" onClick={this.signinClick} disabled={this.state.formInprogress}>Sign in</button>
              </div>
            </div>
          </form>
          {preloader}
        </div>

        <div className="col-sm-6">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
        </div>

      </div>
    </div>)
  }
}

module.exports = Page;
