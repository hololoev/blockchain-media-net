
const Reflux = require('reflux');

const Actions = Reflux.createActions([
  'loadChainInfo',
]);

module.exports = Actions;
