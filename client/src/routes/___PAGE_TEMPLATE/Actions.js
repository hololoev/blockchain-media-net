
const Reflux = require('reflux');

const Actions = Reflux.createActions([
  'dataLoad',
]);

module.exports = Actions;
