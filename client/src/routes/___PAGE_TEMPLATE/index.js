
const React = require('react');
const Reflux = require('reflux');

const Store = require('./Store');

class Page extends Reflux.Component {

  constructor(props) {
    super(props);
    this.store = Store;
  }

  render() {
    return (<div>Hello, {this.props.name}!</div>)
  }
}

module.exports = Page;
