
const Sequelize = require('sequelize');

module.exports = {
  appName: 'medeos.api',
  server: {
    host: '0.0.0.0',
    port: 3030
  },
  logLVL: 'debug',
  db: {
    username: 'root',
    password: 'qwerty',
    database: 'dockerdb',
    host: 'mysql',
    dialect: 'mysql',
    dialectOptions: {
      multipleStatements: true
    },
    logging:false,
    operatorsAliases: Sequelize.Op
  },
  blockchain: {
    type: 'EOS',
    minblock: 37773216,
    apiUrl: [
      'http://jungle2.cryptolions.io:80',
      'https://jungle2.cryptolions.io:443',
      'https://api.jungle.alohaeos.com:443',
      'http://145.239.133.201:8888',
      'http://jungle.eoscafeblock.com:8888',
      'http://jungle2.eosdac.io:8882',
      'https://jungle.eosn.io:443',
      'https://eos-jungle.eosblocksmith.io:443',
      'http://jungle.eosbcn.com:8080'
    ],
    contract: 'bcmedianet11'
  }
};
