'use strict';

const random = require('random');

const config = require('./config');
const modelsLoader = require('./src/libs/modelsLoader');
const eosTools = require('./src/libs/eosTools');

const process_ID = `${process.pid}-${new Date()}`;
const poolSize = 10; // depends on threads count

async function getBlock(blocksModel, blocknum=null) {
  
  let blocksPull;
  
  if( blocknum )
    blocksPull = await blocksModel.findAll({ where: { blockchain_id: blocknum } });
  else
    blocksPull = await blocksModel.findAll({
      where: { status: blocksModel.statuses.INIT.id },
      limit: poolSize,
      order: [ [ 'blockchain_id', 'DESC' ] ]
    });

  if( (blocksPull.length == 0) && (blocknum === null) )
    return false; // nothing toprocess
  
  if( (blocksPull.length == 0) && (blocknum !== null) ) {
    let block = await blocksModel.create({
      status: blocksModel.statuses.INPROGRESS.id,
      thread_id: process_ID,
      updated_at: new Date(),
      blockchain_id: blocknum
    });
    
    return block.dataValues;
  }
  
  let block = blocksPull[ random.int(0, blocksPull.length-1) ].dataValues;

  await blocksModel.update({
    status: blocksModel.statuses.INPROGRESS.id,
    thread_id: process_ID,
    updated_at: new Date()
  }, { where: { blockchain_id: block.blockchain_id } });

  return block;
}

async function savePosts(postModel, posts) {
  for(let trx of posts) {
    let alreadyStored = await postModel.findOne({ where: { blockchain_trx_id: trx.blockchain_trx_id } });

    if( !alreadyStored ) {
      console.log('PUSH TRX:', trx);
      try {
        await postModel.create(trx);
      } catch(err) {
        console.log('Cant create transaction:', trx);
        console.log('ERR:', err);
      }
    } else {
      console.log('TRX ID', trx.blockchain_trx_id, 'already created');
    }
  }
}

async function saveComments(commentsModel, comments) {
  for(let trx of comments) {
    let alreadyStored = await commentsModel.findOne({ where: { blockchain_trx_id: trx.blockchain_trx_id } });

    if( !alreadyStored ) {
      console.log('PUSH TRX:', trx);
      try {
        await commentsModel.create(trx);
      } catch(err) {
        console.log('Cant create transaction:', trx);
        console.log('ERR:', err);
      }
    } else {
      console.log('TRX ID', trx.blockchain_trx_id, 'already created');
    }
  }
}

async function pause(interval) {
  return new Promise( function(resolve) {
    setTimeout(function() {
      return resolve();
    }, interval);
  });
}

function extractPosts(blockchainInfo, blockID) {
  let result = [];
  
  for(let trx of blockchainInfo.transactions) {
    if( typeof(trx.trx) == 'object' )
      if( (trx.trx.transaction.actions[ 0 ].account == config.blockchain.contract) &&
      (trx.trx.transaction.actions[ 0 ].name == 'post')
      ) {
        let threadData;
        try {
          threadData = JSON.parse(trx.trx.transaction.actions[ 0 ].data.threadData);
        } catch(err) {
          console.log('Cannot parse threadData', blockID, trx.trx.transaction.actions[ 0 ].data.threadData);
        }
      
        if( threadData )
          result.push({
            blockchain_user: trx.trx.transaction.actions[ 0 ].data.user,
            blockchain_trx_id: trx.trx.id,
            blockchain_block_id: blockID,
            title: threadData.title || '',
            body: JSON.stringify(threadData.body),
            lang: threadData.lang || 'EN',
            tags: threadData.tags || '',
            type: threadData.type || null,
            category: threadData.category || ''
          });
      }
  }
  
  return result;
}

function extractComments(blockchainInfo, blockID) {
  let result = [];
  
  for(let trx of blockchainInfo.transactions) {
    if( typeof(trx.trx) == 'object' ) {      
      if( (trx.trx.transaction.actions[ 0 ].account == config.blockchain.contract) &&
        (trx.trx.transaction.actions[ 0 ].name == 'comment')
      ) {        
        let commentData;
        try {
          commentData = JSON.parse(trx.trx.transaction.actions[ 0 ].data.commentData);
        } catch(err) {
          console.log('Cannot parse commentData', blockID, trx.trx.transaction.actions[ 0 ].data.commentData);
        }
        
        if( commentData )
          result.push({
            blockchain_user: trx.trx.transaction.actions[ 0 ].data.user,
            blockchain_trx_id: trx.trx.id,
            blockchain_block_id: blockID,
            post_blockchain_block_id: commentData.post_blockchain_block_id, //TODO
            post_blockchain_trx_id: trx.trx.transaction.actions[ 0 ].data.postTRXID,
            parent_blockchain_block_id: commentData.parent_blockchain_block_id || null, //TODO
            parent_blockchain_trx_id: trx.trx.transaction.actions[ 0 ].data.parentTRXID || null,
            body: commentData.body,
            file: commentData.file,
            file_name: commentData.file_name,
            file_type: commentData.file_type
          });
      }
    }
  }
  
  return result;
}

async function processBlocks(db, blocknum=null) {

  const blocksModel = db.models.blocks;
  const postModel = db.models.posts;
  const commentModel = db.models.comments;
  
  let curBlock = await getBlock(blocksModel, blocknum);

  if( curBlock === false)
    return false; // need to pause processing

  console.log('Fetch block: ', curBlock.blockchain_id);
  let blockchainInfo = await eosTools.getBlock(curBlock.blockchain_id);
  
  if( blockchainInfo.result === false ) {
    await blocksModel.update({ status: blocksModel.statuses.ERROR.id }, { where: { blockchain_id: curBlock.blockchain_id } });
    console.log('EOS REQUEST FAILED, BLOCK', curBlock.blockchain_id, 'MARKED AS ERRORED');
    return true;
  }

  let posts = extractPosts(blockchainInfo.response, curBlock.blockchain_id);  
  let comments = extractComments(blockchainInfo.response, curBlock.blockchain_id);
  
  await savePosts(postModel, posts);
  await saveComments(commentModel, comments);
  await blocksModel.update({ status: blocksModel.statuses.DONE.id }, { where: { blockchain_id: curBlock.blockchain_id } });
  console.log('DONE:', curBlock.blockchain_id, `POSTS: ${posts.length}, COMMENTS: ${comments.length}`);
  return true;
}

async function main() {
  const db = await modelsLoader(config.db);
  
  if( process.argv[ 2 ] ) {
    await processBlocks(db, parseInt(process.argv[ 2 ]));
    process.exit();
  }

  
  while(true) {// eslint-disable-line
    let result = await processBlocks(db);
    if( result === true ) {
      // counter ++
    } else {
      console.log('WAIT for fresh blocks');
      await pause(5000);
    }
  }

}

main();
