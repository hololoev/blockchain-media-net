const util = require('util');
const eosjs =  require('eosjs');
const fetch = require('node-fetch');
const JsSignatureProvider = require('eosjs/dist/eosjs-jssig').JsSignatureProvider;

const user = {
  publick: 'EOS8DorxGMFqPWd8DuCFMv9MtiuCQsj2WkmXfFPpZY15xFmwxLg5s',
  priivate: '5JGwstdCwyiEh7pCfPGcRdqFcR8ZW1b2cpTjwE8TWRF15Xz3BFb',
  name: 'medeostest11'
};

const eosNode = 'http://jungle2.cryptolions.io:80';
const contractName = 'medeosthread';

async function main() {
  console.log('*** START ***');

  const signatureProvider = new JsSignatureProvider([ user.priivate ]);
  const rpc = new eosjs.JsonRpc(eosNode, { fetch });
  const api = new eosjs.Api({ rpc, signatureProvider, textDecoder: new util.TextDecoder(), textEncoder: new util.TextEncoder() });

  let payload = '';
  for(let i=0; i<50000; i++)
    payload += '#';

  let trxData = JSON.stringify({ data: payload });

  console.log('*** PAYLOAD SIZE:', trxData.length);

  let eosResult = await api.transact({
    actions: [
      {
        account: contractName,
        name: 'create',
        data: {
          owner: user.name,
          threadData: trxData
        },
        authorization: [
          {
            actor: user.name,
            permission: 'active',
          }
        ]
      }
    ]
  },
  {
    blocksBehind: 3,
    expireSeconds: 30,
  });

  console.log('* RESULT:', eosResult);

  console.log('*** STOP ***');
}

main();
