#!/usr/bin/env node
'use strict';

const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const filepaths = require('filepaths');
const Sequelize = require('sequelize');
const HapiSwagger = require('hapi-swagger');
const AuthBearer = require('hapi-auth-bearer-token');
const hapiBoomDecorators = require('hapi-boom-decorators');


const config = require('./config');
const Package = require('./package');
const Logger = require('./src/libs/Logger');
const bearerValidation = require('./src/libs/bearerValidation');
const buildResult = require('./src/libs/controllersHelpers').buildResult;

const swaggerOptions = {
  jsonPath: '/api/documentation.json',
  documentationPath: '/api/documentation',
  info: {
    title: Package.description,
    version: Package.version
  },
};

async function createServer(logLVL=config.logLVL) {
  // Инициализируем сервер
  const server = await new Hapi.Server(config.server);
  const logger = new Logger(logLVL, config.appName);
  config.db.logging = sql => logger.sqlLog(sql);

  await server.register([
    AuthBearer,
    hapiBoomDecorators,
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    },
    {
      plugin: require('hapi-sequelizejs'),
      options: [
        {
          name: config.db.database, 
          models: [__dirname + '/src/models/*.js'], // Путь к моделькам
          //ignoredModels: [__dirname + '/server/models/**/*.js'], // Можем некоторые модельки заигнорить
          sequelize: new Sequelize(config.db), // Инициализируем обычный секьюлайз и передаём его параметром
          sync: true, // Синхронизировать/нет модели с реальной бд
          forceSync: false, // Если тру, то таблицы будут дропнуты перед синхронизацией, остарожно
        },
      ],
    }
  ]);
  
  server.auth.strategy('token', 'bearer-access-token', {
    allowQueryToken: false,
    unauthorized: bearerValidation.unauthorized,
    validate: bearerValidation.validate
  });

  let routes = filepaths.getSync(__dirname + '/src/routes/');
  for(let route of routes)
    server.route( require(route) );
  
  server.ext({
    type: 'onRequest',
    method: async function (request, h) {
      request.server.config = Object.assign({}, config);
      request.server.logger = logger;
      return h.continue;
    }
  });

  server.ext('onPreResponse', function (request, h) {
    const response = request.response;

    // request.route name
    if ( !response.isBoom ) {
      return h.continue;
    }

    // Wrap error to general response JSON format
    const error = response;

    let errorStatus = error.output.statusCode === 401 ? 'AuthError' : 'ServerError';
    let errorMessage = error.message;

    // Joi response validation error
    if (error.name === 'ValidationError') {
      errorStatus = 'ServerError';
      errorMessage = 'Something wrong happen while server response';

    // Joi request validation error
    } else if (error.name === 'RequestValidationError') {
      errorStatus = 'ValidationError';

    // Other server internal error
    } else if (error.data !== null && typeof error.data !== 'undefined') {
      errorStatus = error.data.internalStatusCode;
    }

    let responseObj = buildResult([], 0, 0, 0, { message: errorMessage, status: errorStatus });

    // Enable stack trace for error responses only for development env
    if (logger.logLVL == logger.logLVLS[ 'debug' ]) {
      responseObj.meta.error.details = error.stack;
    }

    const errExtra = {
      desc: 'Error description from onPreResponse hook handler',
      err_msg: error.message,
      err_stack: error.stack
    };

    logger.error('error', config.appName, errExtra);

    return h.response(responseObj).code(request.response.output.statusCode);
  });

  server.events.on('log', (event, tags) => {
    logger.serverError(tags, event.data, event.channel);
  });

  server.events.on('response', (request) => {
    logger.log('info', 'masternode-backend-route', {
      method: request.route.method,
      url: request.url.href,
      statusCode: request.raw.res.statusCode,
      remoteAddr: request.headers[ 'x-real-ip' ],
      referrer: request.info.referrer
    });
  });
  
  try {
    await server.start();
    logger.log('info', config.appName, `Server running at: ${server.info.uri}`);
  } catch(err) {
    logger.serverError('app', JSON.stringify(err));
  }

  return server;
}

createServer();
