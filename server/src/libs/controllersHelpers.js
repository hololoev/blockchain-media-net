function buildResult(data, total, count, offset, error) {
  return {
    data,
    meta: { total, count, offset, error }
  };
}

module.exports = { buildResult };