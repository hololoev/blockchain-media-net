'use strict';

const util = require('util');
const eosjs =  require('eosjs');
const random = require('random');
const fetch = require('node-fetch');
const JsSignatureProvider = require('eosjs/dist/eosjs-jssig').JsSignatureProvider;

const config = require('../../config');

async function getBlock(blockID) {
  const eosAPINode = config.blockchain.apiUrl[ random.int(0, config.blockchain.apiUrl.length-1) ];
  const rpc = new eosjs.JsonRpc(eosAPINode, { fetch });

  let response;
  try {
    response = await rpc.get_block(blockID);
  } catch(err) {
    return {
      result: false,
      error: err,
      response: null,
      meta: {}
    };
  }

  return {
    result: true,
    error: null,
    response: response,
    meta: {}
  };
}

async function callContract(contractName, EOSUser, method, params={}) {
  const signatureProvider = new JsSignatureProvider([ EOSUser.eos_private ]);
  const eosAPINode = config.blockchain.apiUrl[ random.int(0, config.blockchain.apiUrl.length-1) ];
  const rpc = new eosjs.JsonRpc(eosAPINode, { fetch });
  const api = new eosjs.Api({ rpc, signatureProvider, textDecoder: new util.TextDecoder(), textEncoder: new util.TextEncoder() });
  
  let eosResult;

  try {
    eosResult = await api.transact({
      actions: [
        {
          account: contractName,
          name: method,
          data: params,
          authorization: [
            {
              actor: EOSUser.eos_name,
              permission: 'active'
            }
          ]
        }
      ]
    },
    {
      blocksBehind: 3,
      expireSeconds: 30,
    });
  } catch(err) {
    return {
      result: false,
      error: err,
      response: null,
      meta: {}
    };
  }
  
  let response;
  
  try {
    response = JSON.parse(eosResult.processed.action_traces[ 0 ].console);
  } catch(err) {
    return {
      result: false,
      error: err,
      response: response,
      meta: {}
    };
  }
  
  return {
    result: true,
    error: null,
    response: response,
    meta: {
      transaction_id: eosResult.transaction_id,
      block_num: eosResult.processed.block_num,
      block_time: eosResult.processed.block_time
    }
  };
}

async function getInfo() {
  const eosAPINode = config.blockchain.apiUrl[ random.int(0, config.blockchain.apiUrl.length-1) ];
  const rpc = new eosjs.JsonRpc(eosAPINode, { fetch });
  
  let response;
  
  try {
    response = await rpc.get_info();
  } catch(err) {
    return {
      result: false,
      error: err,
      response: null,
      meta: {}
    };
  }
  
  return {
    result: true,
    error: null,
    response: response,
    meta: {}
  };
}

module.exports = {
  getBlock,
  callContract,
  getInfo
};
