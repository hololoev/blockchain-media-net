
const Joi = require('joi');

const schema = Joi.object({
  account_name: Joi.string(),
  head_block_num: Joi.number().integer(),
  head_block_time: Joi.date(),
  privileged: Joi.boolean(),
  last_code_update: Joi.date(),
  created: Joi.date(),
  core_liquid_balance: Joi.string(),
  ram_quota: Joi.number().integer(),
  net_weight: Joi.number().integer(),
  cpu_weight: Joi.number().integer(),
  net_limit: Joi.object({
    used: Joi.number().integer(),
    available: Joi.number().integer(),
    max: Joi.number().integer()
  }),
  cpu_limit: Joi.object({
    used: Joi.number().integer(),
    available: Joi.number().integer(),
    max: Joi.number().integer()
  }),
  ram_usage: Joi.number().integer(),
  permissions: Joi.array(),
  total_resources: Joi.any(),
  self_delegated_bandwidth: Joi.any(),
  refund_request: Joi.any(),
  voter_info: Joi.any()
});

module.exports = schema;
