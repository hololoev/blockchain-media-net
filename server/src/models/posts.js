'use strict';

module.exports = (sequelize, DataTypes) => {
  const posts = sequelize.define('posts', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    blockchain_user: {
      type: DataTypes.STRING
    },
    blockchain_trx_id: {
      type: DataTypes.STRING
    },
    blockchain_block_id: {
      type: DataTypes.INTEGER
    },
    title: {
      type: DataTypes.STRING
    },
    body: {
      type: DataTypes.TEXT('long')
    },
    lang: {
      type: DataTypes.STRING
    },
    tags: {
      type: DataTypes.STRING
    },
    type: {
      type: DataTypes.STRING
    },
    category: {
      type: DataTypes.STRING
    },
  });

  posts.dummyData = [];

  return posts;
};
