
const Joi = require('joi');

const metaSchema = require('../../libs/response_schemes/meta');
const buildResult = require('../../libs/controllersHelpers').buildResult;

const userSchema = require('../../libs/response_schemes/user');
const accessTokenSchema = require('../../libs/response_schemes/accessToken');

async function response(request) {

  let exportUser = {
    id: request.auth.artifacts.user.id,
    email: request.auth.artifacts.user.email,
    eos_name: request.auth.artifacts.user.eos_name,
    eos_public: request.auth.artifacts.user.eos_public,
    createdAt: request.auth.artifacts.user.createdAt,
    updatedAt: request.auth.artifacts.user.updatedAt,
  };

  return buildResult([ { token: request.auth.artifacts.dbToken, user: exportUser } ], 1, 1, 0, null);
}

const responseSchema = Joi.object({
  meta: metaSchema,
  data: Joi.array().items({
    token: accessTokenSchema,
    user: userSchema
  })
});

module.exports = {
  method: 'GET',
  path: '/api/auth',
  options: {
    handler: response,
    description: 'Token confirmation',
    notes: 'Token confirmation',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      headers: Joi.object({
        authorization: Joi.string().required().description('Authorization token').example('Bearer d585649bad7421a9b6c84531bad992cc')
      }).options({ allowUnknown: true })
    },
    response: { schema: responseSchema }
  }
};
