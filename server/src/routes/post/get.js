
const Joi = require('joi');
const Op = require('sequelize').Op;

const metaSchema = require('../../libs/response_schemes/meta');
const buildResult = require('../../libs/controllersHelpers').buildResult;
const queryHelper = require('../../libs/queryHelper');
const postSchema = require('../../libs/response_schemes/post');

async function response(request) {

  const posts = request.getModel(request.server.config.db.database, 'posts');

  let queryParams = queryHelper.parseQueryParams(request.query);

  if( request.query.username )
    queryParams.where.blockchain_user = request.query.username;

  if( request.query.query )
    queryParams.where.body = {
      [ Op.like ]: `%${request.query.query}%`
    };

  let resultPosts = await posts.findAll(queryParams);

  let result = [];
  for(let item of resultPosts)
    result.push(item.get({ plain: true }));

  return buildResult(result, 1, result.length, 0, null);
}

const responseSchema = Joi.object({
  meta: metaSchema,
  data: Joi.array().items(postSchema)
});

module.exports = {
  method: 'GET',
  path: '/api/post',
  options: {
    handler: response,
    description: 'Get list of posts',
    notes: 'Get list of posts',
    tags: [ 'api' ],
    auth: false,
    validate: {
      query: {
        __order: Joi.string().allow(null).default('').description('Query order').example('createdAt desc').default('createdAt desc'),
        __count: Joi.number().integer().allow(null).min(1).max(100).default(20).description('Necessary rows count').example(10),
        __offset: Joi.number().integer().allow(null).min(0).default(0).description('Offset').example(0),
        username: Joi.string().allow(null).description(`User's blockchain name`).example('Ololoev'),
        query: Joi.string().allow(null).description(`Search query`).example('Mememe bebebe')
      }
    },
    response: { schema: responseSchema }
  }
};
