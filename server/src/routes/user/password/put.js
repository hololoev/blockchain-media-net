
const Joi = require('joi');
const Boom = require('boom');

const buildResult = require('../../../libs/controllersHelpers').buildResult;
const metaSchema = require('../../../libs/response_schemes/meta');
const okSchema = require('../../../libs/response_schemes/okSchema');

const MIN_PASS_LENGTH = 3;

function checkForm(payload) {
  if( payload.password.length < MIN_PASS_LENGTH )
    return 'Password is too short';

  if( payload.password !== payload.confirmation )
    return 'Password and confirmation do not match';

  return false;
}

async function response(request) {

  const users = request.getModel(request.server.config.db.database, 'users');

  let validationResult = checkForm(request.payload);
  if( validationResult !== false ) {
    throw Boom.badRequest(validationResult);
  }

  if( !users.verifyPassword(request.payload.currentPassword, request.auth.artifacts.user.password) ) {
    throw Boom.unauthorized('Wrong password');
  }

  let newRecord = {
    password: users.hashPassword(request.payload.password),
  };
  
  await users.update(newRecord, { where: { id: request.auth.artifacts.user.id } });

  return buildResult([ { result: 'ok' } ], 1, 1, 0, null);
}

const responseSchema = Joi.object({
  meta: metaSchema,
  data: Joi.array().items(okSchema)
});

module.exports = {
  method: 'PUT',
  path: '/api/user/password',
  options: {
    handler: response,
    description: 'Change user password',
    notes: 'Change user password',
    tags: [ 'api' ],
    auth: 'token',
    validate: {
      payload: {
        currentPassword: Joi.string().required().example('123456'),
        password: Joi.string().required().example('qwerty'),
        confirmation: Joi.string().required().example('qwerty')
      }
    },
    response: { schema: responseSchema }
  }
};
