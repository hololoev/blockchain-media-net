'use strict';

const eosTools = require('../libs/eosTools');

const onePasssLimit = 1000;

async function createEOSBlocks(models, config) {

  const blocks = models.models.blocks;

  let chainInfo = await eosTools.getInfo();
  
  if( !chainInfo.result ) {
    return ;// eos rpc error
  }

  let maxQusery = `SELECT MAX(blockchain_id) max_id, MIN(blockchain_id) min_id FROM blocks`;
  let dbResult = await models.query(maxQusery);
  
  let values = [];
  for(let i=dbResult[ 0 ][ 0 ].min_id-1; i>config.blockchain.minblock; i--) {
    values.push({
      blockchain_id: i,
      status: blocks.statuses.INIT.id,
    });
    if( values.length > onePasssLimit )
      break;
  }
  
  for(let i=dbResult[ 0 ][ 0 ].max_id+1; i<chainInfo.response.last_irreversible_block_num; i++) {
    values.push({
      blockchain_id: i,
      status: blocks.statuses.INIT.id,
    });
    if( values.length > onePasssLimit )
      break;
  }
  
  await blocks.bulkCreate(values);
  return values.length;
}

module.exports = createEOSBlocks;
